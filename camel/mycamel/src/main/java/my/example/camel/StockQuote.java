package my.example.camel;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

public class StockQuote {
		
	private String stockCode;
	private double percentage;
	private double percentageFromLast;
	private double percentageFromOpen;
	private double volumePercentage;
	private double volumePercentageFromLast;
	private double change;
	private double last;
	private double previousClose;
	private double open;
	private double high;
	private double low;
	private long volume;
	private long volumeAverage;
	private Date time;
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	public double getPercentageFromLast() {
		return percentageFromLast;
	}
	public void setPercentageFromLast(double percentageFromLast) {
		this.percentageFromLast = percentageFromLast;
	}
	public double getPercentageFromOpen() {
		return percentageFromOpen;
	}
	public void setPercentageFromOpen(double percentageFromOpen) {
		this.percentageFromOpen = percentageFromOpen;
	}
	public double getVolumePercentage() {
		return volumePercentage;
	}
	public void setVolumePercentage(double volumePercentage) {
		this.volumePercentage = volumePercentage;
	}
	public double getVolumePercentageFromLast() {
		return volumePercentageFromLast;
	}
	public void setVolumePercentageFromLast(double volumePercentageFromLast) {
		this.volumePercentageFromLast = volumePercentageFromLast;
	}
	public double getChange() {
		return change;
	}
	public void setChange(double change) {
		this.change = change;
	}
	public double getLast() {
		return last;
	}
	public void setLast(double last) {
		this.last = last;
	}
	public double getPreviousClose() {
		return previousClose;
	}
	public void setPreviousClose(double previousClose) {
		this.previousClose = previousClose;
	}
	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public double getHigh() {
		return high;
	}
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}
	public void setLow(double low) {
		this.low = low;
	}
	public long getVolume() {
		return volume;
	}
	public void setVolume(long volume) {
		this.volume = volume;
	}
	public long getVolumeAverage() {
		return volumeAverage;
	}
	public void setVolumeAverage(long volumeAverage) {
		this.volumeAverage = volumeAverage;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
		
}
