package my.example.camel;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

public class CsvStringToStockQuoteBean {
	public static StockQuote convert(String stockQuoteCsvStr){ 
		StockQuote stockQuote = new StockQuote();
		
		String[] data = StringUtils.split(stockQuoteCsvStr, ",");
		stockQuote.setStockCode(data[0].trim());
		stockQuote.setPercentage(Double.parseDouble(data[1].trim()));
		stockQuote.setPercentageFromLast(Double.parseDouble(data[2].trim()));
		stockQuote.setPercentageFromOpen(Double.parseDouble(data[3].trim()));
		stockQuote.setVolumePercentage(Double.parseDouble(data[4].trim()));
		stockQuote.setVolumePercentageFromLast(Double.parseDouble(data[5].trim()));
		stockQuote.setChange(Double.parseDouble(data[6].trim()));
		stockQuote.setLast(Double.parseDouble(data[7].trim()));
		stockQuote.setPreviousClose(Double.parseDouble(data[8].trim()));
		stockQuote.setOpen(Double.parseDouble(data[9].trim()));
		stockQuote.setHigh(Double.parseDouble(data[10].trim()));
		stockQuote.setLow(Double.parseDouble(data[11].trim()));
		stockQuote.setVolume(Long.parseLong(data[12].trim()));
		stockQuote.setVolumeAverage(Long.parseLong(data[13].trim()));
		try {
			stockQuote.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data[14]));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		
		return stockQuote;
	}
}
