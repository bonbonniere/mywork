package my.example;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.junit.Test;

public class CsvCamelTest {
	
	@Test
	public void testCsv() throws Exception {
		CamelContext context = new DefaultCamelContext();
		context.getProperties().put("http.proxyHost", "localhost");
		context.getProperties().put("http.proxyPort", "8888");
		
        context.addRoutes(new RouteBuilder() {
            public void configure() {
            	from("file:data/inbox/?fileName=stockwatch.txt&noop=true").
                unmarshal().csv()
                .split(body())
//                .bean(new StockCsvToObjectHandler())
                .marshal().json(JsonLibrary.Jackson)
                .log("... ${body}")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .to("http://localhost:8080/RestService/stockQuote/saveJSON");
            }
        });
        context.start();
        Thread.sleep(5000);
        context.stop();
	}
}
