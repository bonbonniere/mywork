package my.example;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.Test;

public class RestCamelTest {
	
	@Test
	public void testCsv() throws Exception {
		CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() {
            	from("direct:start")
                //.setHeader(Exchange.HTTP_METHOD, constant("POST"))
            	.log("..... ${body}")
                .to("http://localhost:8080/RestService/stockQuote/show/1");
            }
        });
        context.start();
        Thread.sleep(5000);
        context.stop();
	}
}
