package my.example;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import org.junit.Test;

public class MailTest {
	
	@Test
	public void testMail() {
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
		  Session session = Session.getDefaultInstance(props, null);
		  Store store = session.getStore("imaps");
		  store.connect("imap.gmail.com", "yaoappemail@gmail.com", "compliance");
		  Folder folder=store.getDefaultFolder();
		  Folder inboxfolder=folder.getFolder("INBOX");
		  inboxfolder.open(Folder.READ_ONLY);
		  Message[] msgs=inboxfolder.getMessages();
		  for (Message msg : msgs) {
			  System.out.println("subject:" + msg.getSubject());
		  }
		} catch (NoSuchProviderException e) {
		  e.printStackTrace();
		  System.exit(1);
		} catch (MessagingException e) {
		  e.printStackTrace();
		  System.exit(2);
		}
	}
}
