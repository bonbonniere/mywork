package my.example;

import org.apache.camel.CamelContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations="classpath:META-INF/spring/camel-context.xml")  
public class MyRouteBuilderTest {
	
	@Autowired
	CamelContext context;
	
	@Test
	public void testRoute() throws Exception {
		context.start();
        Thread.sleep(10000);

        // stop the CamelContext
        context.stop();
	}

}
