package my.example;

import java.util.Map;

import javax.activation.DataHandler;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mail.SplitAttachmentsExpression;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.Test;

import com.icegreen.greenmail.pop3.Pop3Server;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;

public class GreenMailTest {

	@Test
	public void testRetrieve() throws Exception {
		GreenMail greenMail = new GreenMail(ServerSetupTest.ALL);  //
		greenMail.setUser("test@localhost.com", "password");
	    greenMail.start();
	    
	    Pop3Server pop3Server = greenMail.getPop3();
	    System.out.println(pop3Server.getBindTo());
	    System.out.println(pop3Server.getPort());
	    
	    //use random content to avoid potential residual lingering problems
	    final String subject = GreenMailUtil.random();
	    final String body = GreenMailUtil.random();
	    System.out.println(subject + " : " + body);
	    //GreenMailUtil.sendTextEmailTest("test@localhost.com", "from@localhost.com", subject, body);
	    GreenMailUtil.sendAttachmentEmail("test@localhost.com", "from@localhost.com", subject, body, 
	    		"test, csv, content".getBytes(), "text/csv", "stockwatch.txt", "test", ServerSetupTest.SMTP);
	    
	    //wait for max 5s for 1 email to arrive
	    //assertTrue(greenMail.waitForIncomingEmail(5000, 1));
	    
	    //receive code
//	    assertEquals(subject, greenMail.getReceivedMessages()[0].getSubject());
	    
	    //java receive code
	    //sets POP3 properties
//	    Properties properties = new Properties();
//	    properties.put("mail.pop3.host", "localhost");
//	    properties.put("mail.pop3.port", 3110);
//	    Session session = Session.getDefaultInstance(properties);
//	    session.setDebug(true);
//	    POP3Store emailStore = (POP3Store) session.getStore("pop3");
//	    emailStore.connect("localhost", 3110, "test", "password");
//	    Folder emailFolder = emailStore.getFolder("INBOX");
//	    emailFolder.open(Folder.READ_ONLY);
//	    Message[] messages = emailFolder.getMessages();
//	    for (int messageCount = 0; messageCount < messages.length; messageCount++) {
//	        Message aMessage = messages[messageCount];
//	        String senderAddress = aMessage.getFrom()[0].toString();
//	        String subject1 = aMessage.getSubject();
//	        System.out.println("subject: " + subject1);
//	    }
	    
	    CamelContext context = new DefaultCamelContext();
	    context.setTracing(true);
        context.addRoutes(new RouteBuilder() {
            public void configure() {
            	from("pop3://test@localhost:3110?username=test@localhost.com&password=password&consumer.delay=1000&delete=true")
            		.split(new SplitAttachmentsExpression())
            		.process(new Processor(){
						@Override
						public void process(Exchange exchange) throws Exception {
//							System.out.println("processor is running...");
//							System.out.println("body...:" + arg0.getIn().getBody().toString());
//							System.out.println("subject..." + arg0.getIn().getHeader("Subject"));
							Map<String, DataHandler> attachments = exchange.getIn().getAttachments();
					        if (attachments.size() > 0) {
					            for (String name : attachments.keySet()) {
					                DataHandler dh = attachments.get(name);
					                // get the file name
					                String filename = dh.getName();

					                // get the content and convert it to byte[]
					                byte[] data = exchange.getContext().getTypeConverter()
					                                  .convertTo(byte[].class, dh.getInputStream());
					                
					                System.out.println("attachment content...:" + new String(data));
					            }
					        }
						}
            			
            		})
	    			.to("log:email");
            }
        });
        context.start();
        Thread.sleep(3000);
        context.stop();
	}
}
