package my.example;

import my.example.camel.CsvStringToStockQuoteBean;
import my.example.camel.ListAggregationStrategy;
import my.example.camel.MyMailProcessor;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.Test;


public class MailCamelTest {
	
	@Test
	public void test() throws Exception {
        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
        	MyMailProcessor attachmentProcessor = new MyMailProcessor();
            public void configure() {
            	from("imaps://imap.gmail.com?username=yaoapptest@gmail.com&password=compliance"
            		    + "&delete=false&unseen=false&consumer.delay=60000")
            		    .process(attachmentProcessor)
            		    .to("log:afterProcess")
            		    .split(body(String.class).tokenize("\n"), new ListAggregationStrategy())
            		    	.to("log:afterSplit")
            		    	.bean(new CsvStringToStockQuoteBean())
            		    	.to("log:convertToJavaObject")
            		    .end()
            		    .to("log:afterAggregate")
            		    .setHeader(Exchange.HTTP_METHOD, constant("POST"))
            		    .to("http://localhost:8080/RestService/stockQuote/show/1")
            		    ;
            }
        });
        context.start();
        Thread.sleep(10000);
        context.stop();
	}
}
