package my.algorithm.tree.lca;

public class BinaryTree {
	//Root of the Binary Tree
	private Node root;
	
	private int count; //to calcualte complexity
	
	public BinaryTree(Node root) {
		this.root = root;
	}
	
	public Node getRoot() {
		return root;
	}

	public Node findLCA(int n1, int n2)
	{
		count = 0;
		Node node = findLCA(root, n1, n2);
		System.out.println("complexity: " + count);
		return node;
	}

	// This function returns pointer to LCA of two given
	// values n1 and n2. This function assumes that n1 and
	// n2 are present in Binary Tree
	public Node findLCA(Node node, int n1, int n2)
	{
		count++;
		// Base case
		if (node == null)
			return null;

		// If either n1 or n2 matches with root's key, report
		// the presence by returning root (Note that if a key is
		// ancestor of other, then the ancestor key becomes LCA
		if (node.getData() == n1 || node.getData() == n2) {
			return node;
		}

		// Look for keys in left and right subtrees
		Node left_lca = findLCA(node.getLeft(), n1, n2);
		Node right_lca = findLCA(node.getRight(), n1, n2);
		log(left_lca, right_lca);
		
		// If both of the above calls return Non-NULL, then one key
		// is present in once subtree and other is present in other,
		// So this node is the LCA
		if (left_lca!=null && right_lca!=null)
			return node;

		// Otherwise check if left subtree or right subtree is LCA
		return (left_lca != null) ? left_lca : right_lca;
	}
	
	private void log(Node node1, Node node2) {
		System.out.println("node1 " + getNode(node1));
		System.out.println("node2 " + getNode(node2) + "\n");
	}
	
	private String getNode(Node node) {
		if (node == null) {
			return "NULL";
		} else {
			return String.valueOf(node.getData());
		}
	}
}
