package my.algorithm.tree.lca;

public class Node {
	private int data;
	private Node left, right;

	public Node(int item)
	{
		data = item;
		left = right = null;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

	public int getData() {
		return data;
	}

}
