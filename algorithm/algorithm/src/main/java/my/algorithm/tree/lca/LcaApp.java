package my.algorithm.tree.lca;

/*
 * It is to find the lowest common Ancestor
 * the tree is like this 
 *             1
 * 			2     3
 *        4   5  6  7
 *        
 *  Let' say the total node number is n
 *  The worst time complexity is O(n+(n-1)), so it is O(n)         
 */
public class LcaApp {
	public static void main(String[] args) {		
		BinaryTree tree = new BinaryTree(new Node(1));
		Node root = tree.getRoot();
		
		root.setLeft(new Node(2));
		root.setRight(new Node(3));
		root.getLeft().setLeft(new Node(4));
		root.getLeft().setRight(new Node(5));
		root.getRight().setLeft(new Node(6));
		root.getRight().setRight(new Node(7));
		System.out.println("LCA(4, 5) = " + tree.findLCA(4, 5).getData());
		
//		System.out.println("LCA(4, 6) = " +
//							tree.findLCA(4, 6).getData());
//		System.out.println("LCA(3, 4) = " +
//							tree.findLCA(3, 4).getData());
//		System.out.println("LCA(2, 4) = " +
//							tree.findLCA(2, 4).getData());
	}
}
