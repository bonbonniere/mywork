CREATE TABLE PURCHASE_ORDER (
	id INT NOT NULL AUTO_INCREMENT,
	total DECIMAL NOT NULL, 
	PRIMARY KEY (id)
);

CREATE TABLE LINE_ITEM (
	id INT NOT NULL AUTO_INCREMENT,
	subtotal DECIMAL NOT NULL,
	quantity INT NOT NULL,
	product varchar(30) NOT NULL,
	order_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (order_id) REFERENCES PURCHASE_ORDER (id)
);
