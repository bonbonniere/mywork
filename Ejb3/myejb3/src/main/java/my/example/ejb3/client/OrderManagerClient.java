package my.example.ejb3.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import my.example.ejb3.entity.LineItem;
import my.example.ejb3.entity.Order;
import my.example.ejb3.stateless.OrderManager;

public class OrderManagerClient {
	private static OrderManager orderManager;

	public static void main(String[] args) {
		try {
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			env.put(Context.PROVIDER_URL, "jnp://localhost:1099");
			env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
			 
			Context context = new InitialContext(env);
			//String jndiName = "myejb3-0.0.1-SNAPSHOT/" + OrderManager.class.getSimpleName() + "/remote";
			String jndiName = "OrderManager111/remote";
			orderManager = (OrderManager) context.lookup(jndiName);
			
			Order order = generateNewTestOrder();
			
			order = orderManager.createOrder(order);

			System.out.println("Order created with Order ID: " + order.getId());
			
			order.setTotal(99.99);

			orderManager.updateOrder(order);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private static Order generateNewTestOrder() {
		Order order = new Order();
		order.setTotal(1);
		
		Collection<LineItem> lineItems = new ArrayList<LineItem>();
		
		LineItem lineItem = new LineItem();
		lineItem.setOrder(order);
		lineItem.setProduct("TEST PRODUCT");
		lineItem.setQuantity(9);
		lineItem.setSubtotal(100);
		
		lineItems.add(lineItem);
		
		order.setLineItems(lineItems);

		return order;
	}
}
