package my.example.ejb3.stateless;

import javax.ejb.Remote;

import my.example.ejb3.entity.Order;

@Remote
public interface OrderManager {
	public Order createOrder(Order order);

	public Order updateOrder(Order order);
	
	public void deleteOrder(Order order);
}