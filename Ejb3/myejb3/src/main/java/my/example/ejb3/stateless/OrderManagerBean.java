package my.example.ejb3.stateless;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import my.example.ejb3.entity.Order;

@Stateless(name = "OrderManager111")
public class OrderManagerBean implements OrderManager {
	@PersistenceContext(unitName = "myEjb3")
	private EntityManager entityManager;

	public Order createOrder(Order order) {
		entityManager.persist(order);
		return order;
	}

	public Order updateOrder(Order order) {
		entityManager.merge(order);

		return order;
	}

	public void deleteOrder(Order order) {
		entityManager.remove(entityManager.merge(order));
	}
}