package my.example.ejb3;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class MyStatelessEjb
 */
@Stateless
@LocalBean
public class MyStatelessEjb {

    /**
     * Default constructor. 
     */
    public MyStatelessEjb() {
        // TODO Auto-generated constructor stub
    }
    
    public String sayHello(String name) { 	
        return "Hello " + name + "!"; 	
    }

}
