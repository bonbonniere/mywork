var mongoose = require('../database/mongoose');

var Product = mongoose.model('Product', 
	{ 
		name: String, 
		price: String,  //TODO Double
		description: String
	});

exports.save = function (obj, callBack) {
	console.log("save to mongo db... " + obj);
	var product = new Product(obj);
	product.save(function (err, savedProduct) {
			if (err) throw err;
			callBack(savedProduct);
		});
};

exports.findAll = function (callBack) {
	Product.find({}, function(err, products) {
		if (err) throw err;
		callBack(products);
	});
};

exports.findById = function(id, callBack) {
	Product.findById(id, function(err, product) {
		if (err) throw err;
		callBack(product);
	});
};

exports.update = function(id, obj, callBack) {
	Product.findByIdAndUpdate(id, obj, {new: true}, function(err, product){
		if (err) throw err;
		callBack(product);
	});
};

exports.delete = function(id, callBack) {
	Product.findByIdAndRemove(id, function(err) {
		if (err) throw err;
		// we have deleted the user
		callBack("product " + id + " was deleted...");
	});
};