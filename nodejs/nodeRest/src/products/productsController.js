var product = require('./productsEntity');

function ProductsController() {{
	this.get = function (req, res, next) {
		console.log('rest get method is executed...');
		product.findAll(function(result) {
			res.send(result);
		});
		return next();
	};
	this.getById = function (req, res, next) {
		console.log('rest getById method is executed... id:' + req.params.id);
		product.findById(req.params.id, function(result){
			res.send(result);
		});
		return next();
	};
	this.post = function (req, res, next) {
		console.log('rest post method is executed... ' + JSON.stringify(req.body));
		product.save(req.body, function(result){
			res.send(result);
		});
		return next();
	};
	this.put = function (req, res, next) {
		console.log('rest put method is executed...+id:' + req.params.id);
		product.update(req.params.id, req.body, function(result){
			res.send(result);
		});
		return next();
	};
	this.del = function (req, res, next) {
		console.log('rest del method is executed... id:' + req.params.id);
		product.delete(req.params.id, function(result){
			res.send(result);
		});
		return next();
	};

}}

module.exports = new ProductsController();