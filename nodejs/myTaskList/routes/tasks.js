var express = require('express');
var router = express.Router();

const taskEntity = require('../server/task/taskEntity');

router.get('/tasks', function(req, res, next){
	taskEntity.findAllNewTasks(function(tasks){
		res.json(tasks);
	});
});

router.get('/tasks/completed', function(req, res, next){
	taskEntity.findAllCompletedTasks(function(tasks){
		res.json(tasks);
	});
});

router.get('/task/:id', function(req, res, next){
	taskEntity.findById(req.params.id, function(task){
		res.json(task);
	});
});

router.post('/task', function(req, res, next){
	var task = req.body;
	taskEntity.save(task, function(task) {
		res.json(task);
	});
});

router.delete('/task/:id', function(req, res, next){
	taskEntity.delete(req.params.id, function(result){
		res.json(result);
	})
});

router.put('/task/:id', function(req, res, next){
	var toUpdate = req.body;
	taskEntity.update(req.params.id, toUpdate, function(task) {
		res.json(task);
	});
});

router.put('/task/moveToTop/:id', (req, res, next) => {
	taskEntity.moveToTop(req.params.id, (updatedTask) => {
		res.json(updatedTask);
	});
});

router.put('/task/moveToBottom/:id', (req, res, next) => {
	taskEntity.moveToBottom(req.params.id, (updatedTask) => {
		res.json(updatedTask);
	});
});

router.put('/task/moveUp/:id', (req, res, next) => {
	taskEntity.moveUp(req.params.id, (updatedTask) => {
		res.json(updatedTask);
	});
});

router.put('/task/moveDown/:id', (req, res, next) => {
	taskEntity.moveDown(req.params.id, (updatedTask) => {
		res.json(updatedTask);
	});
});

router.put('/task/reopen/:id', (req, res, next) => {
	taskEntity.reopen(req.params.id, (task) => {
		res.json(task);
	});
});

module.exports = router;
