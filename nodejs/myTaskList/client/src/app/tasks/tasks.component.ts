import { Component, OnInit } from '@angular/core';
import { TasksService } from '../tasks.service';
// import { Router } from '@angular/router';
//import {Task} from 'task';
import { Nl2brPipe } from '../nl2br.pipe';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
	tasks: any = [];
	completedTasks : any = [];
	task: any;

	constructor(private tasksService: TasksService) { } //, private router: Router

	ngOnInit() {
		this.tasksService.getAllTasks()
						.subscribe(tasks => {
							this.tasks = tasks;
						});
		this.tasksService.getAllCompletedTasks()
						.subscribe(completedTasks => {
							this.completedTasks = completedTasks;
						});				
		this.task = {id: '', content:'', priority:'', status:''};
	}

	getBlankNewTask() {
		return {id: '', content:'', priority:'', status:''};
	}

	getAllTasks()  {
		this.tasksService.getAllTasks()
						.subscribe(tasks => {
							this.tasks = tasks;
						});
	}

	getAllCompletedTasks() {
		this.tasksService.getAllCompletedTasks()
			.subscribe( completedTasks => {
				this.completedTasks = completedTasks;
			});
	}

	saveTask() {
		if (this.task.status == '') {
			this.task.status = 'new';
		}
		if (this.task.priority =='') {
			this.task.priority = 1;
		}
		this.tasksService.saveTask(this.task)
			.subscribe(task => {
				this.getAllTasks();	
				this.task = this.getBlankNewTask();
				// this.router.navigate(['/']);
			});
		
	}

	deleteTask(taskId) {
		this.tasksService.deleteTask(taskId)
			.subscribe( data => {
				this.getAllTasks();	
			});
	}

	getTask(taskId) {
		this.tasksService.getTaskById(taskId)
			.subscribe( (task) => {
				this.task = task;
			});
	}

	completeTask(taskId) {
		this.tasksService.completeTask(taskId).subscribe(data => {
				this.getAllTasks();
				this.getAllCompletedTasks();
			});
	}

	reopenTask(taskId) {
		this.tasksService.reopenTask(taskId).subscribe(data => {
				this.getAllTasks();
				this.getAllCompletedTasks();
			});
	}


	moveToTop(taskId) {
		this.tasksService.moveToTop(taskId).subscribe(data => {
			this.getAllTasks();
		})
	}

	moveToBottom(taskId) {
		this.tasksService.moveToBottom(taskId).subscribe(data => {
			this.getAllTasks();
		})
	}

	moveUp(taskId) {
		this.tasksService.moveUp(taskId).subscribe(data => {
			this.getAllTasks();
		})
	}

	moveDown(taskId) {
		this.tasksService.moveDown(taskId).subscribe(data => {
			this.getAllTasks();
		})
	}
}
