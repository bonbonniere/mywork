import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TasksService {

	constructor(private http: Http) { }

	// Get all posts from the API
	getAllTasks() {
		return this.http.get('/api/tasks')
			.map(res => res.json());  //return observable
	}

	getAllCompletedTasks() {
		return this.http.get('/api/tasks/completed')
			.map(res => res.json()); 
	}

	saveTask(task) {
		var headers = new Headers();
		headers.append("Content-Type", "application/json");
		if (task._id) {
			return this.http.put('/api/task/' + task._id, JSON.stringify(task), {headers: headers})
				.map(res => res.json());	
		} else {
			return this.http.post('/api/task', JSON.stringify(task), {headers: headers})
				.map(res => res.json());
		}
	}

	getTaskById(taskId) {
		return this.http.get('/api/task/' + taskId)
			.map(res => res.json());
	}

	completeTask(taskId) {
		var newStatus = {status: 'completed'};
		var headers = new Headers();
		headers.append("Content-Type", "application/json");
		return this.http.put('/api/task/' + taskId, JSON.stringify(newStatus), {headers: headers})
			.map(res => res.json());	
	}

	reopenTask(taskId) {
		return this.http.put('/api/task/reopen/' + taskId, '', {})
			.map(res => res.json());
	}

	deleteTask(taskId) {
		return this.http.delete('/api/task/' + taskId)
			.map(res => res.json());
	}

	moveUp(taskId) {
		return this.http.put('/api/task/moveUp/' + taskId, '', {})
			.map(res => res.json());
	}

	moveDown(taskId) {
		return this.http.put('/api/task/moveDown/' + taskId, '', {})
			.map(res => res.json());
	}

	moveToTop(taskId) {
		return this.http.put('/api/task/moveToTop/' + taskId, '', {})
			.map(res => res.json());	
	}

	moveToBottom(taskId) {
		return this.http.put('/api/task/moveToBottom/' + taskId, '', {})
			.map(res => res.json());	
	}

}
