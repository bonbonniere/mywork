var env = process.env.NODE_ENV || 'dev';

var config = {
    dev: {
        mongo: {
            url: 'mongodb://localhost:27017/mytask'
        }
    },
    production: {
        mongo: {
            url: process.env.MONGO_URI
        }
    }
};

module.exports = config[env];