var mongoose = require('../database/mongoose');

var Task = mongoose.model('Task', 
	{ 
		content: String,
		priority: Number, 
		status: String, 
		lastUpdateTime: { type: Date, default: Date.now } 
	});

var findMaxPriority = (callBack) => {
	Task.aggregate([{$match:{'status':'new'}}, {$group: {_id:"all", maxPriority:{$max:"$priority"}}}], (err, result) => {
		callBack(result);
	});
};

var findMinPriority = (callBack) => {
	Task.aggregate([{$match:{'status':'new'}}, {$group: {_id:"all", minPriority:{$min:"$priority"}}}], (err, result) => {
		console.log("findMaxPriority:" + JSON.stringify(result));
		callBack(result);
	});
};

exports.save = function (obj, callBack) {
	findMaxPriority((result) => {
		if (result == null || result.length == 0) {
			obj.priority = 10000;
		} else {
			obj.priority = result[0].maxPriority + 100;
		}
		var task = new Task(obj);
		console.log(task);
		task.save(function (err, savedTask) {
			if (err) throw err;
			callBack(savedTask);
		});
	});
};

exports.reopen = function(taskId, callBack) {
	findMaxPriority((result) => {
		var priority = 10000;
		if (result != null || result.length == 0) {
			priority = result[0].maxPriority + 100;
		}
		var toUpdate = {priority:priority, status:'new'};
		exports.update(taskId, toUpdate, callBack);
	});
};

exports.findAllNewTasks = function (callBack) {
	Task.find({status:'new'})
		.sort({priority : 1})
		.exec(function(err, tasks) {
			if (err) throw err;
			callBack(tasks);
		});
};

exports.findAllCompletedTasks =  function (callBack) {
	Task.find({status:'completed'}, function(err, tasks) {
		if (err) throw err;
		callBack(tasks);
	});
};

exports.findById = function(id, callBack) {
	Task.findById(id, function(err, task) {
		if (err) throw err;
		callBack(task);
	});
};

exports.update = function (id, obj, callBack) {
	delete obj['_id'];
	delete obj['lastUpdateTime'];
	Task.findByIdAndUpdate(id, obj, function(err, result) {
		if (err) throw err;
		callBack(result);
	});
};

exports.delete = function(id, callBack) {
	Task.findByIdAndRemove(id, function(err, result) {
		if (err) throw err;
		callBack(result);
	});
}; 

exports.moveToTop = function(id, callBack) {
	findMinPriority((result) => {
		var toUpdate = {};
		if (result == null || result.length == 0) {
			toUpdate.priority = 10000;
		} else {
			toUpdate.priority = result[0].minPriority - 100;
		}
		Task.findByIdAndUpdate(id, toUpdate, {new: true}, function(err, updatedTask){
			callBack(updatedTask);
		});
	});
};

exports.moveToBottom = function(id, callBack) {
	findMaxPriority((result) => {
		var toUpdate = {};
		if (result == null || result.length == 0) {
			toUpdate.priority = 10000;
		} else {
			toUpdate.priority = result[0].maxPriority + 100;
		}
		Task.findByIdAndUpdate(id, toUpdate, {new: true}, function(err, updatedTask){
			callBack(updatedTask);
		});
	});
};

var exchangePriority = (leftTask, rightTask) => {
	// console.log('before exchange priority............');
	// console.log(leftTask);
	// console.log(rightTask);
	var leftPriority = leftTask.priority;
	leftTask.priority = rightTask.priority;
	rightTask.priority = leftPriority;
	// console.log('after exchange priority............');
	// console.log(leftTask);
	// console.log(rightTask);
	return [leftTask, rightTask];
};

exports.moveUp = function(id, callBack) {
	exports.findById(id, (selectedTask) => {
		Task.find({status:'new', priority:{$lt:selectedTask.priority}})
			.sort({priority : -1})
			.limit(1)
			.exec((err, previousTask) => {
				if (previousTask.length == 0) {
					callBack(selectedTask);
				} else {
					var tasks = exchangePriority(previousTask[0], selectedTask);
					Task.findByIdAndUpdate(tasks[0]._id, tasks[0], {new: true}, function(err, updatedTask){
						Task.findByIdAndUpdate(tasks[1]._id, tasks[1], {new: true}, function(err, task){
							callBack(task);
						});
					});
				}
			})
	});
};

exports.moveDown = function(id, callBack) {
	exports.findById(id, (selectedTask) => {
		Task.find({status:'new', priority:{$gt:selectedTask.priority}})
			.sort({priority : 1})
			.limit(1)
			.exec((err, nextTask) => {
				if (nextTask.length == 0) {
					callBack(selectedTask);
				} else {
					var tasks = exchangePriority(nextTask[0], selectedTask);
					Task.findByIdAndUpdate(tasks[0]._id, tasks[0], {new: true}, function(err, updatedTask){
						Task.findByIdAndUpdate(tasks[1]._id, tasks[1], {new: true}, function(err, task){
							callBack(task);
						});
					});
				}
			})
	});
};
