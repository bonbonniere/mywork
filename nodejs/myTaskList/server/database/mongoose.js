var mongoose = require('mongoose');
var envConfig = require('../config/envConfig');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function() {
    console.log('connection with database: ' + envConfig.mongo.url + ' was successfully.');
});

mongoose.connect(envConfig.mongo.url);

process.on('SIGINT', function() {
	mongoose.connection.close(function () {
		console.log('Mongoose disconnected on app termination');
		process.exit(0);
	});
});

module.exports = mongoose;