var React = require('react')
var ReactDOM = require('react-dom');
var Redux = require('redux');
var $ = require('jquery');

var stateReducer = function (state, action) {
	switch (action.type) {
		case 'BLUE_CLICKED' : 
			return $.extend(true, {}, state, {name:'blue'});
		case 'RED_CLICKED' : 
			return $.extend(true, {}, state, {name:'red'});
		case 'ELSE_CLICKED'	:	
			return $.extend(true, {}, state, {elseProp:'test else property...'});
		default : 
			return {name:'...', elseProp: ''}; 
	}
}

var state2Reducer = function (state, action) {
	switch (action.type) {
		case 'CHANGE_ANOTHER_STATE' : 
			return $.extend(true, {}, state, {anotherState:'anotherState'});
		default : 
			return {anoterhState: 'state2'}; 
	}
}

var store = Redux.createStore(stateReducer);
var store2 = Redux.createStore(state2Reducer);

//========================================

class Hello extends React.Component {
	render() {
		console.log("Hello render()...");
		return <div>
					Hello, {this.props.name}!
					<div>
						<button onClick={function(){store.dispatch({type:'BLUE_CLICKED'});}}>Blue</button> 
						<span> </span>
						<button onClick={function(){store.dispatch({type:'RED_CLICKED'});}}>Red</button>
						<span> </span>
						<button onClick={function(){store.dispatch({type:'ELSE_CLICKED'});}}>Something Else</button>
						<span> </span>
						<button onClick={function(){store2.dispatch({type:'CHANGE_ANOTHER_STATE'});}}>Another State</button>
					</div>
				</div>
	}
	componentDidUpdate(prop, state) {
		console.log('Hello componentDidUpdate...');
	}
}

class Else extends React.Component {
	render() {
		return <div>
			   	<h2>{this.props.elseProp}</h2>
			   	<h4>{this.props.anotherState}</h4>
			   </div>
	}
}

var renderHello = function() {
	ReactDOM.render(
	    <Hello name={store.getState().name} />,
	    document.getElementById('container')
	);
};

var renderElse = function() {
	ReactDOM.render(
	    <Else elseProp={store.getState().elseProp} anotherState={store2.getState().anotherState}/>,
	    document.getElementById('elseDiv')
	);
}

store.subscribe(renderHello);
store.subscribe(renderElse);
store2.subscribe(renderElse);

var render = function() {
	renderHello();
	renderElse();
}

module.exports = render;