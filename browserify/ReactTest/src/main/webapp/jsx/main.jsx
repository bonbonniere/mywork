var FilterableProductTable=require('./reactProducts.jsx');
var product = require('../js/products.js');
var ReactDOM = require('react-dom');
var React = require('react');
ReactDOM.render(
    <FilterableProductTable products={product()} />,
    document.getElementById('container')
);