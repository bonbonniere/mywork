var browserify = require('browserify'),
    watchify = require('watchify'),
    reactify = require('reactify'),
    gulp = require('gulp'),
    source = require('vinyl-source-stream'),
    sourceFile = 'src/main/webapp/jsx/main.jsx',
    destFolder = 'src/main/webapp/js/build/',
    destFile = 'main.js';

gulp.task('prod', function() {
    var bundler = browserify({
        entries: [sourceFile], // Only need initial file, browserify finds the deps
        transform: [reactify], // We want to convert JSX to normal javascript
        debug: true, // Gives us sourcemapping
        cache: {}, packageCache: {}, fullPaths: true // Requirement of watchify
    }).bundle() 
    .on('error', function(err){
      // print the error (can replace with gulp-util)
      console.log(err.message);
      // end this stream
      this.emit('end');
    })
    .pipe(source(destFile))
    .pipe(gulp.dest(destFolder));
});
gulp.task('dev', function() {
	var bundler = browserify({
        entries: [sourceFile], // Only need initial file, browserify finds the deps
        transform: [reactify], // We want to convert JSX to normal javascript
        debug: true, // Gives us sourcemapping
        cache: {}, packageCache: {}, fullPaths: true // Requirement of watchify
    });
    var watcher  = watchify(bundler);

    return watcher
    .on('update', function () { // When any files update
        var updateStart = Date.now();
        watcher.bundle() // Create new bundle that uses the cache for high performance
        .on('error', function(err){
        // print the error (can replace with gulp-util)
        console.log(err.message);
        // end this stream
        this.emit('end');
      })
        .pipe(source(destFile))
    // This is where you add uglifying etc.
        .pipe(gulp.dest(destFolder));
        console.log('Updated!', (Date.now() - updateStart) + 'ms');
    })
    .bundle() // Create the initial bundle when starting the task
    .on('error', function(err){
        // print the error (can replace with gulp-util)
        console.log(err.message);
        // end this stream
        this.emit('end');
      })
    .pipe(source(destFile))
    .pipe(gulp.dest(destFolder));
});
gulp.task('default', ['dev']);