package my.example.ejb2;

import java.util.Properties;

import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;

import my.example.ejb2.spring.TestEjbSpring;
import my.example.ejb2.spring.TestEjbSpringRemoteHome;

public class JbossEjbClient {

	public static void main(String[] args) {

		System.out.println("===========================================================");
		Properties props = new Properties();
		props.setProperty("java.naming.factory.initial",
				"org.jnp.interfaces.NamingContextFactory");
		props.setProperty("java.naming.provider.url", "localhost:1099");
		props.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming");
		try {
			InitialContext ctx = new InitialContext(props);

			Object objRef = ctx.lookup("ejb/TestStatelessSessionBean");

			TestEjbRemoteHome home = (TestEjbRemoteHome) PortableRemoteObject.narrow(objRef, TestEjbRemoteHome.class);

			TestEjb remote = home.create();

			remote.sayHello();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try {
			InitialContext ctx = new InitialContext(props);

			Object objRef = ctx.lookup("ejb/TestStatelessSessionBeanSupportSpring");

			TestEjbSpringRemoteHome springEjbHome = (TestEjbSpringRemoteHome) PortableRemoteObject.narrow(objRef, TestEjbSpringRemoteHome.class);

			TestEjbSpring springEjbRemote = springEjbHome.create();

			springEjbRemote.sayHello();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
