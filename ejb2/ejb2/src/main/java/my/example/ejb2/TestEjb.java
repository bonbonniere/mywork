package my.example.ejb2;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface TestEjb extends EJBObject {
	public void sayHello() throws RemoteException;;
}
