package my.example.ejb2.spring;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import org.springframework.ejb.support.AbstractStatelessSessionBean;

public class TestEjbSpringBean extends AbstractStatelessSessionBean{

	private static final long serialVersionUID = 1L;
	
	private SpringMockService springMockService;

	public void ejbCreate() throws EJBException {
		System.out.println("ejb create");
	}
	
	protected void onEjbCreate() throws CreateException {
		springMockService = (SpringMockService) getBeanFactory().getBean("springMockService");
	}

	public void sayHello() {
		System.out.println("hello spring");
		springMockService.run();
	}

}
