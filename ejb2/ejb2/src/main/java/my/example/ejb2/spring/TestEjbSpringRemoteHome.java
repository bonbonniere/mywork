package my.example.ejb2.spring;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface TestEjbSpringRemoteHome extends EJBHome {
	TestEjbSpring create() throws RemoteException, CreateException;
}
