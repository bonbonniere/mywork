package my.example.ejb2;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface TestEjbRemoteHome extends EJBHome {
	TestEjb create() throws RemoteException, CreateException;
}
