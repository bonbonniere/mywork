package my.example.ejb2.spring;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface TestEjbSpring extends EJBObject {
	public void sayHello() throws RemoteException;;
}
