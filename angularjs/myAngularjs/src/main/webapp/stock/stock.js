(function(angular) {

	'use strict';

	angular.module('stock', ['yahooQuote']).controller('StockController', ['yahooFindQuote', function(yahooFindQuote){
		var stock = this;
		this.quote = {};
		
		this.findQuote = function() {
			console.log(stock.quote);
			
			stock.quote.results = [];
			stock.quote.historyResults = [];
			
			yahooFindQuote.findQuote(stock.quote);
		};
		
	}]);

})(window.angular);

