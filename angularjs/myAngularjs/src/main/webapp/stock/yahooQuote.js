(function(angular) {
	'use strict';
	var app = angular.module('yahooQuote', []);
	app.factory('yahooFindQuote', ['$http', function($http) {
		var env = '&env=store://datatables.org/alltableswithkeys';
		var format = '&format=json';
		var callBack= '&callback=JSON_CALLBACK';
		
		var findQuote = function(quote) {
			if (quote.startDate && quote.endDate) {
				findHistoryQuote(quote);
			} else {
				findRealTimeQuote(quote);
			}
		};
		
		var findRealTimeQuote = function (quote) {
			var yql = 'select * from yahoo.finance.quote where symbol ="SYMBOL_TEXT"';
			var yqlStr = yql.replace("SYMBOL_TEXT", quote.symbol);
			var YAHOO_FINANCE_URL = '//query.yahooapis.com/v1/public/yql?q=' + yqlStr + format + env + callBack;
			return $http.jsonp(YAHOO_FINANCE_URL).success(function(data) {
				quote.results = [].concat(data.query.results.quote);
				console.log(quote.results);
			});
		};
		
		var findHistoryQuote = function(quote) {
			var yqlHistory = 'select * from yahoo.finance.historicaldata where symbol = "SYMBOL_TEXT" and startDate = "START_DATE" and endDate = "END_DATE" | sort(field="Date")';
			var yqlStr = yqlHistory.replace("SYMBOL_TEXT", quote.symbol).replace("START_DATE", quote.startDate).replace("END_DATE", quote.endDate);
			var YAHOO_FINANCE_URL = '//query.yahooapis.com/v1/public/yql?q=' + yqlStr + format + env + callBack;
			console.log(YAHOO_FINANCE_URL);
			return $http.jsonp(YAHOO_FINANCE_URL).success(function(data) {
				quote.historyResults = [].concat(data.query.results.quote);
				console.log(quote.historyResults);
			});
		};
		
		return {
			findQuote : findQuote
		};
		
	}]);
})(window.angular);